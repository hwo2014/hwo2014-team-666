#pragma once
#include "parseJSON.h"
#include <math.h>

//call this second lap to collect data
double run_until_crash(carPos *prev_pos, track *track, bool crash);

//call this function every car pos tick for the first lap if no other cars
void collect_data(carPos *carPos, track *track);



//calcs the current speed from the current position and last ticks position.
double get_speed(carPos *prev_pos, carPos* curr_pos, track *track);


//calcs the length of a piece on the selected lane
double calculate_length(track *car_track, int pieceIndex, int laneIndex);