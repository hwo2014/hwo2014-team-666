#pragma once
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"

#define PI 3.14159265359

typedef struct track {
    int *trackLength; // length of track in pieces
    double *length; // (angle * PI * radius) / 180 = arc length
    double *angle;
    double *radius;
    bool *isSwitch;
    bool *isBridge;

    // lane information (array index = lane index, value = offset)
    int *distancesFromCenter;
    int *nrOfLanes;
} track;

typedef struct carPos {
    char *name;
    char *color;

    double *angle;
    int *gameTick;

    int *pieceIndex;
    double *inPieceDistance;

    int *startLaneIndex;
    int *endLaneIndex;

    int *lap;
} carPos;

typedef struct lapFinished {
	char *name;
	char *color;
	
	int *lap;
	int *lapTicks;
	int *lapMillis;
	
	int *raceLaps;
	int *raceTicks;
	int *raceMillis;

	int *rankOverall;
	int *rankFastest;
} lapFinished;

track *parse_track(cJSON *object);
carPos **parse_car(cJSON *obect);
lapFinished *lap_info(cJSON *object);
char *parse_color(cJSON *object);
void free_track(track *carTrack);
void free_pos(carPos *carPosition);
void free_lap(lapFinished *lap);
