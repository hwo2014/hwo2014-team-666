#include "parseJSON.h"

// the cJSON object should include only the tag "track". this means
// that the original "gameInit" will have to be parsed by "race"
track *parse_track(cJSON *object) {
	int length = 0;
	int nrOfLanes = 0;

	cJSON *data = cJSON_GetObjectItem(object, "data");
	cJSON *race = cJSON_GetObjectItem(data, "race");

	cJSON *JTrack = cJSON_GetObjectItem(race, "track");
	cJSON *JPieces = cJSON_GetObjectItem(JTrack, "pieces");
	cJSON *JLanes = cJSON_GetObjectItem(JTrack, "lanes");

	length = cJSON_GetArraySize(JPieces);
	nrOfLanes = cJSON_GetArraySize(JLanes);

	track *theTrack = malloc(sizeof(track));
	theTrack->trackLength = malloc(sizeof(int));
	theTrack->length = malloc(sizeof(double) * length);
	theTrack->angle = malloc(sizeof(double) * length);
	theTrack->radius = malloc(sizeof(double) * length);
	theTrack->isSwitch = malloc(sizeof(bool) * length);
	theTrack->isBridge = malloc(sizeof(bool) * length);
	theTrack->distancesFromCenter = malloc(sizeof(int) * nrOfLanes);
	theTrack->nrOfLanes = malloc(sizeof(int));

	*(theTrack->trackLength) = length;

	int i;
	for (i = 0; i < length; i++) {
		cJSON *piece = cJSON_GetArrayItem(JPieces, i);
	   
		 // attempt to get the "length" attribute from the piece, if that fails
		// the piece is curved.
		// failed cJSONs return null? is this a good way of doing things?
		if (cJSON_GetObjectItem(piece, "length") != NULL) {
			theTrack->length[i] = cJSON_GetObjectItem(piece, "length")->valuedouble;
			theTrack->angle[i] = 0;
		} else {
			int radius = cJSON_GetObjectItem(piece, "radius")->valueint;
			double angle = cJSON_GetObjectItem(piece, "angle")->valuedouble;
			theTrack->length[i] = (angle * PI * (double) radius) / 180;
			theTrack->angle[i] = angle;
			theTrack->radius[i] = radius;
		}

		if (cJSON_GetObjectItem(piece, "switch") != NULL) {
			theTrack->isSwitch[i] = true;
		} else {
			theTrack->isSwitch[i] = false;
		}

	}

	*(theTrack->nrOfLanes) = nrOfLanes;

	for (i = 0; i < nrOfLanes; i++) {
		cJSON *lane = cJSON_GetArrayItem(JLanes, i);
		int j = cJSON_GetObjectItem(lane, "index")->valueint;
		theTrack->distancesFromCenter[j] = cJSON_GetObjectItem(lane, "distanceFromCenter")->valueint;
	}

	return theTrack;
}

// the cJSON object should be the entire "carPositions" object
// including the "data" tag
carPos **parse_car(cJSON *object) {
	int i, nrOfCars;

	cJSON *carList = cJSON_GetObjectItem(object, "data");
	nrOfCars = cJSON_GetArraySize(carList);

	carPos **parsedList = malloc(sizeof(carPos *) * nrOfCars);

	for (i = 0; i < nrOfCars; i++) {
		carPos *tempCar = malloc(sizeof(carPos));
		cJSON *JCar = cJSON_GetArrayItem(carList, i);

		cJSON *JId = cJSON_GetObjectItem(JCar, "id");
		if (cJSON_GetObjectItem(JId, "name") != NULL) {
			tempCar->name = malloc(sizeof(cJSON_GetObjectItem(JId, "name")->valuestring));
			strcpy(tempCar->name, cJSON_GetObjectItem(JId, "name")->valuestring);
		}
		if (cJSON_GetObjectItem(JId, "color") != NULL) {
			tempCar->color = malloc(sizeof(cJSON_GetObjectItem(JId, "color")->valuestring));
			strcpy(tempCar->color, cJSON_GetObjectItem(JId, "color")->valuestring);
		}

		tempCar->angle = malloc(sizeof(double));
		tempCar->pieceIndex = malloc(sizeof(int));
		tempCar->inPieceDistance = malloc(sizeof(double));
		tempCar->startLaneIndex = malloc(sizeof(int));
		tempCar->endLaneIndex = malloc(sizeof(int));
		tempCar->lap = malloc(sizeof(int));
		tempCar->gameTick = malloc(sizeof(int));

		*(tempCar->angle) = cJSON_GetObjectItem(JCar, "angle")->valuedouble;

		cJSON *JPiece = cJSON_GetObjectItem(JCar, "piecePosition");
		*(tempCar->pieceIndex) = cJSON_GetObjectItem(JPiece, "pieceIndex")->valueint;
		*(tempCar->inPieceDistance) = cJSON_GetObjectItem(JPiece, "inPieceDistance")->valuedouble;

		cJSON *JLane = cJSON_GetObjectItem(JPiece, "lane");
		*(tempCar->startLaneIndex) = cJSON_GetObjectItem(JLane, "startLaneIndex")->valueint;
		*(tempCar->endLaneIndex) = cJSON_GetObjectItem(JLane, "endLaneIndex")->valueint;

		*(tempCar->lap) = cJSON_GetObjectItem(JPiece, "lap")->valueint;
		if (cJSON_GetObjectItem(object, "gameTick") != NULL) {
			*(tempCar->gameTick) = cJSON_GetObjectItem(object, "gameTick")->valueint;
		} else {
			*(tempCar->gameTick) = 0;
		}

		parsedList[i] = tempCar;
   }

	return parsedList;
}

lapFinished *lap_info(cJSON *object) {
	lapFinished *lf = malloc(sizeof(lapFinished));

	cJSON *lapinfo = cJSON_GetObjectItem(object, "data");
	
	cJSON *car = cJSON_GetObjectItem(lapinfo, "car");

	lf->name = malloc(sizeof(cJSON_GetObjectItem(car, "name")->valuestring));
	strcpy(lf->name, cJSON_GetObjectItem(car, "name")->valuestring);

	lf->color = malloc(sizeof(cJSON_GetObjectItem(car, "color")->valuestring));
	strcpy(lf->color, cJSON_GetObjectItem(car, "color")->valuestring);

	cJSON *laptime = cJSON_GetObjectItem(lapinfo, "lapTime");

	*(lf->lap) = cJSON_GetObjectItem(laptime, "lap")->valueint;
	*(lf->lapTicks) = cJSON_GetObjectItem(laptime, "ticks")->valueint;
	*(lf->lapMillis) = cJSON_GetObjectItem(laptime, "millis")->valueint;

	cJSON *racetime = cJSON_GetObjectItem(lapinfo, "raceTime");

	*(lf->raceLaps) = cJSON_GetObjectItem(racetime, "laps")->valueint;
	*(lf->raceTicks) = cJSON_GetObjectItem(racetime, "ticks")->valueint;
	*(lf->raceMillis) = cJSON_GetObjectItem(racetime, "millis")->valueint;

	cJSON *ranking = cJSON_GetObjectItem(lapinfo, "ranking");

	*(lf->rankOverall) = cJSON_GetObjectItem(ranking, "overall")->valueint;
	*(lf->rankFastest) = cJSON_GetObjectItem(ranking, "fastestlap")->valueint;

	return lf;
}

char *parse_color(cJSON *object) {
	cJSON *car = cJSON_GetObjectItem(object, "data");
	char *color = malloc(sizeof(cJSON_GetObjectItem(car, "color")->valuestring));
	strcpy(color, cJSON_GetObjectItem(car, "color")->valuestring);
	return color;
}

void free_track(track *carTrack) {

	free(carTrack->trackLength);
	free(carTrack->length);
	free(carTrack->angle);
	free(carTrack->isSwitch);
	free(carTrack->isBridge);
	free(carTrack->distancesFromCenter);
	free(carTrack);
}

void free_pos(carPos *carPosition){

}

void free_lap(lapFinished *lap) {
	free(lap->name);
	free(lap->color);
	free(lap);
}
