#pragma once
#include "cJSON.h"
#include "parseJSON.h"
#include "datacollector.h"
#include <math.h>

typedef struct driving_data {
	double speed_factor;
	double break_factor;
	//double 
} driving_data;
double calculate_throttle(carPos *car_position, track *car_track, double speed_factor);
carPos* get_own_car_pos(carPos **carPositions, char *carColor, int nr_of_cars);
int should_switch(track *car_track, int current_index, int lane_index);