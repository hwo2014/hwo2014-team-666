#include "brain.h"

double calculate_throttle(carPos *car_position, track *car_track, double speed_factor) {

   double throttle = 0.5;

   int angleoffset = 2;
   int straightoffset = 3;

   int index = *(car_position->pieceIndex);
/*
   if(index+fmax(straightoffset, angleoffset) > *(car_track->trackLength)){
      throttle = 1.0;
   //Straight track
   } else if(car_track->angle[index+angleoffset] < 1.0 &&
      car_track->angle[index+angleoffset] > -1.0 ) {
      throttle = 1.0;
   //Right turn if the car is driving clockwise
   } else if(car_track->angle[index+straightoffset] > 0.0) {
      throttle = 0.4;
   //Left turn
   } else if(car_track->angle[index+straightoffset] < 0.0) {
      throttle = 0.4;
   } else {
      throttle = 0.5;
   }
   */
   return throttle;
}

carPos* get_own_car_pos(carPos **car_positions, char *car_color, int nr_of_cars) {
   carPos *own_car;

   int i = 0;
   for(i = 0; i < nr_of_cars; i++) {
      if(strcmp(car_positions[i]->color, car_color) == 0) {
         own_car = car_positions[i];

      }
   }
   return own_car;
}

int should_switch(track *car_track, int current_index, int lane_index) {

   int nrOfLanes = *(car_track->nrOfLanes);
   double distances[nrOfLanes];
   int i = 0;
   int switchLanes = 0;

   printf("lane_index = %d\n", lane_index);

   while(!car_track->isSwitch[++current_index]) {
      i = 0;
      for(i = 0; i < nrOfLanes; i++) {
         distances[i] += calculate_length(car_track, current_index, i);
      }
   }

   int least_index = 0;
   double least_value = distances[0];
   for(i = 0; i < nrOfLanes; i++) {
      if(distances[i] < least_value) {
         least_index = i;
         least_value = distances[i];
      }
   }

   if(least_index != lane_index) {
      if(lane_index < least_index) {
         switchLanes = 1;
      } else {
         switchLanes = -1;
      }
      
   }


   return switchLanes;

}
