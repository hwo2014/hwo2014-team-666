#include "datacollector.h"



double run_until_crash(carPos *prev_pos, track *track, bool crash){
	/*double crash_angle;
	double piece_angle;
	if(!crash){

	} else if(crash){
		crash_angle = *(prev_pos->angle);
		int index = *(prev_pos->pieceIndex);
		piece_angle = (track->angle[index]);	
		
	}
*/
	return 0.5;
}


//call this function every car pos tick for the first lap if no other cars
void collect_data(carPos *carPos, track *track){

	
	

}



//calcs the current speed from the current position and last ticks position.
double get_speed(carPos *prev_pos, carPos* curr_pos, track *track){
	
	double speed = 0;

	if(prev_pos != NULL) {
		int prev_index = *(prev_pos->pieceIndex);
		int lane = *(prev_pos->startLaneIndex);
	
		if(*(curr_pos->pieceIndex) == *(prev_pos->pieceIndex)){
			
			speed = (*(curr_pos->inPieceDistance) - *(prev_pos->inPieceDistance));

		} else{
			printf("PIECE LENGTH: %lf\n", calculate_length(track, prev_index,lane));
			double prev_piece_dist = calculate_length(track, prev_index,lane)-(*(prev_pos->inPieceDistance));
			double curr_piece_dist = *(curr_pos->inPieceDistance);
	
			speed = prev_piece_dist + curr_piece_dist;
		}

	}
	return speed;
}


//calcs the length of a piece on the selected lane
double calculate_length(track *car_track, int pieceIndex, int laneIndex) {
	double angle = (car_track->angle)[pieceIndex];
	double radius = (car_track->radius)[pieceIndex];
	
	if (angle == 0) {
		return (car_track->length)[pieceIndex];
	} else if (angle > 0) {
		radius -= (car_track->distancesFromCenter)[laneIndex];
	} else if (angle < 0) {
		radius += (car_track->distancesFromCenter)[laneIndex];
	}
	return (abs(angle) * PI * radius) / 180;
}
