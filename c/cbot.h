#pragma once
#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include "parseJSON.h"
#include "cJSON.h"
#include "brain.h"
#include "datacollector.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);
static cJSON *switch_msg(int switch_lane);
static cJSON *create_race(char *bot_name, char *bot_key, char *track_name, char *password, int car_count);
static cJSON *join_race(char *bot_name, char *bot_key, char *track_name, int car_count);