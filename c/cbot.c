#include "cbot.h"

static void error(char *fmt, ...)
{
	char buf[BUFSIZ];

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, BUFSIZ, fmt, ap);
	va_end(ap);

	if (errno)
		perror(buf);
	else
		fprintf(stderr, "%s\n", buf);

	exit(1);
}

static int connect_to(char *hostname, char *port)
{
	int status;
	int fd;
	char portstr[32];
	struct addrinfo hint;
	struct addrinfo *info;

	memset(&hint, 0, sizeof(struct addrinfo));
	hint.ai_family = PF_INET;
	hint.ai_socktype = SOCK_STREAM;

	sprintf(portstr, "%d", atoi(port));

	status = getaddrinfo(hostname, portstr, &hint, &info);
	if (status != 0) error("failed to get address: %s", gai_strerror(status));

	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0) error("failed to create socket");

	status = connect(fd, info->ai_addr, info->ai_addrlen);
	if (status < 0) error("failed to connect to server");

	freeaddrinfo(info);
	return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
	cJSON *msg_data;

	if (!strcmp("join", msg_type_name)) {
		puts("Joined");
	} else if (!strcmp("gameStart", msg_type_name)) {
		puts("Race started");
	} else if (!strcmp("crash", msg_type_name)) {
		puts("Someone crashed");
	} else if (!strcmp("gameEnd", msg_type_name)) {
		puts("Race ended");
	} else if (!strcmp("dnf", msg_type_name)) {
		puts("Disqualifed!");
	} else if (!strcmp("error", msg_type_name)) {
		msg_data = cJSON_GetObjectItem(msg, "data");
		if (msg_data == NULL)
			puts("Unknown error");
		else
			printf("ERROR: %s\n", msg_data->valuestring);
	}
}

/* Do like this to commit changes:
git add cbot.c    - adds the file that should be commited
git commit -m "this is a comment"     - this will commit locally
git push -u origin master       - pushes to master branch on bitbucket

UPDATE: use the push script like this:
./push "comment like this"


to pull from bitbucket use:
git pull --all

*/
int main(int argc, char *argv[])
{
	int sock;
	cJSON *json;
	track *car_track;
	char *car_color;
	bool crashed = false;
	int curr_piece_index = 0;

	double speed = 0.0;

	carPos *prev_car_pos = NULL;
	carPos *curr_car_pos = NULL;

	int nr_of_cars;

	if (argc != 5)
		error("Usage: bot host port botname botkey\n");

	sock = connect_to(argv[1], argv[2]);

	//json = join_msg(argv[3], argv[4]);
	//json = create_race(argv[3], argv[4], "germany", "", 2);
	json = join_race(argv[3], argv[4], "germany", 2);
	write_msg(sock, json);
	cJSON_Delete(json);
	
	while ((json = read_msg(sock)) != NULL) {
		cJSON *msg, *msg_type;
		char *msg_type_name;

		msg_type = cJSON_GetObjectItem(json, "msgType");
		if (msg_type == NULL)
			error("missing msgType field");

		msg_type_name = msg_type->valuestring;

		if (!strcmp("gameInit", msg_type_name)) {
			//TODO: if car_track!=NULL free ??
			car_track = parse_track(json);

			msg = ping_msg();
		} else if (!strcmp("yourCar", msg_type_name)) {
			car_color = parse_color(json);
			msg = ping_msg();
		} else if (!strcmp("carPositions", msg_type_name)) {


			cJSON *car_list = cJSON_GetObjectItem(json, "data");
			nr_of_cars = cJSON_GetArraySize(car_list);
			
			carPos **car_positions = parse_car(json);
			prev_car_pos = curr_car_pos;
			curr_car_pos = get_own_car_pos(car_positions, car_color, nr_of_cars);

			if(prev_car_pos != NULL) {
				speed = get_speed(prev_car_pos, curr_car_pos, car_track);
				//printf("speed: %lf\n", speed);
				//printf("gametick: %d\n", *(curr_car_pos->gameTick));
			} else {
				printf("prev pos null\n");
			}
			
			int index = (*(curr_car_pos->pieceIndex) + 1);

			if(index > *(car_track->trackLength)) {
				index = 0;
			}

			if(car_track->isSwitch[index] && (curr_piece_index != *(curr_car_pos->pieceIndex))){
				msg = switch_msg(should_switch(car_track, (*(curr_car_pos->pieceIndex) + 1), *(curr_car_pos->startLaneIndex)));
			} else {
				msg = throttle_msg(calculate_throttle(curr_car_pos, car_track, 0));
			}
			
			curr_piece_index = *(curr_car_pos->pieceIndex);

//			msg = throttle_msg(0.6568);
		} else if (!strcmp("lapFinished", msg_type_name)) {
//			lapFinished *lf = lap_info(json);

//			free_lap(lf);
			msg = ping_msg();
		} else if (!strcmp("crash", msg_type_name)) {
			printf("Someone crashed\n");
			crashed = true;
			msg = ping_msg();

		} else if (!strcmp("spawn", msg_type_name)) {
			crashed = false;
			msg = ping_msg();
		} else {
			log_message(msg_type_name, json);
			msg = ping_msg();
		}

		write_msg(sock, msg);

		cJSON_Delete(msg);
		cJSON_Delete(json);
	}

//	free_track(car_track);

	return 0;
}

static cJSON *ping_msg()
{
	return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
	cJSON *data = cJSON_CreateObject();
	cJSON_AddStringToObject(data, "name", bot_name);
	cJSON_AddStringToObject(data, "key", bot_key);

	return make_msg("join", data);
}

static cJSON *join_race(char *bot_name, char *bot_key, char *track_name, int car_count)
{
	cJSON *data = cJSON_CreateObject();
	cJSON *bot_id = cJSON_CreateObject();

	cJSON_AddStringToObject(bot_id, "name", bot_name);
	cJSON_AddStringToObject(bot_id, "key", bot_key);

	cJSON_AddItemToObject(data, "botId", bot_id);

	cJSON_AddStringToObject(data, "trackName", track_name);
	cJSON_AddItemToObject(data, "carCount", cJSON_CreateNumber(car_count));

	return make_msg("joinRace", data);
}

static cJSON *create_race(char *bot_name, char *bot_key, char *track_name, char *password, int car_count)
{
	cJSON *data = cJSON_CreateObject();
	cJSON *bot_id = cJSON_CreateObject();

	cJSON_AddStringToObject(bot_id, "name", bot_name);
	cJSON_AddStringToObject(bot_id, "key", bot_key);

	cJSON_AddItemToObject(data, "botId", bot_id);

	cJSON_AddStringToObject(data, "trackName", track_name);
	cJSON_AddStringToObject(data, "password", track_name);
	cJSON_AddItemToObject(data, "carCount", cJSON_CreateNumber(car_count));

	return make_msg("createRace", data);
}

static cJSON *switch_msg(int switch_lane) 
{
	printf("switch %d\n", switch_lane);
	//switch left
	if(switch_lane == -1){
		return make_msg("switchLane", cJSON_CreateString("Left"));
	//Swith right
	} else {
		return make_msg("switchLane", cJSON_CreateString("Right"));
	}
	
}

static cJSON *throttle_msg(double throttle)
{
	return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
	cJSON *json = cJSON_CreateObject();
	cJSON_AddStringToObject(json, "msgType", type);
	cJSON_AddItemToObject(json, "data", data);
	return json;
}

static cJSON *read_msg(int fd)
{
	int bufsz, readsz;
	char *readp, *buf;
	cJSON *json = NULL;

	bufsz = 16;
	readsz = 0;
	readp = buf = malloc(bufsz * sizeof(char));

	while (read(fd, readp, 1) > 0) {
		if (*readp == '\n')
			break;

		readp++;
		if (++readsz == bufsz) {
			buf = realloc(buf, bufsz *= 2);
			readp = buf + readsz;
		}
	}

	if (readsz > 0) {
		*readp = '\0';
		json = cJSON_Parse(buf);
		if (json == NULL)
			error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
	}
	free(buf);
	return json;
}

static void write_msg(int fd, cJSON *msg)
{
	char nl = '\n';
	char *msg_str;

	msg_str = cJSON_PrintUnformatted(msg);

	write(fd, msg_str, strlen(msg_str));
	write(fd, &nl, 1);

	free(msg_str);
}
